package com.cyte.sample.referenceapplication.notification;

import android.content.Context;

@SuppressWarnings({"unused"})
public abstract class SyncPushNotification extends PushNotification {

    protected SyncPushNotification(Context ctx) {
        super(ctx);
    }
}
